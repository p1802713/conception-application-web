# Conception application web
Readme : au fur et à mesure de ce TP, indiquez dans le fichier README.md les différents choix d'implémentation que vous aurez faits, pour les questions où vous avez plusieurs alternatives. Vous y mentionnerez également les détails que vous jugez utiles à connaître pour le correcteur (bibliothèques supplémentaires utilisées, dernière question traitée, etc.).

# TP2

### 1.2 Servlets
Nous avons utilisé une redirection externe pour le servelet deconnexion.

### 1.3 Structuration de l'interface
Pour le pied de page, étant donné qu'il est statique, nous avons fait un fichier footer.html.

Pour le header, étant donné qu'il est dynamique (le titre change en fonction de la page), nous avons créé un fichier .jsp.

Pour le menu, étant donné qu'il est dynamique (les liens changent en fonction de la connexion de l'utilisateur),nous avons créé un fichier .jsp.
Nous avons décider de faire deux test pour l'affichage des liens.
Avec les résultats toujours accessibles sans test :
1) Si l'utilisateur est connecté il a les liens pour voter, voir voir sa preuve de vote et se déconnecter.
2) Si l'utilisateur n'est pas connecté, il peut avoir le lien pour se connecter (surtout pour le cas dans lequel il va sur la page des résultats sans être connecté, puis veut se connecter par la suite)

Afin d'intégrer les sous ressources sur toutes les pages, nous avons décidé que l'index serait aussi un fichier .jsp, pour permettre les includes.

En ce qui concerne les différents titres à afficher, ils sont présent dans le header grace à <<request.getParameter("title")>>. En effet, nous avons choisi de passer le titre par les directives de redirection. Cela permet une meilleure modularité pour le titre en fonction des cas.
Exemple : L'utilsateur a déjà voté mais il vote une seconde fois, dans ce cas il est redirigé vers ballot.jsp avec un titre "Supprimez le vote pour pouvoir revoter". Tandis que si il vote pour la première fois, il est redirigé vers ballot.jsp avec un titre "Votre preuve de vote".

### 1.4 Améliorations des fonctionnalités
Pour pouvoir voter blanc, nous avons rajouté un candidat dans la liste dans le fichier "CandidatListGenrator.java". Ce canditat a le prénom "vote" et le nom "blanc". Cela a permit de ne pas modifier la structure du code existant

Pour pouvoir maj le profil de l'utilisateur nous avons ajouté une page jsp "user.jsp" qui contient un formulaire. Lorsque ce formulaire est POST le traitement de la modification du nom ce fait dans le servelet "Modification.java".

### Autre amélioration

L'utilisateur ne peut plus voter 2 fois. En effet, nous avons ajouté une verification dans le servelet "vote.java".

# Intégration continue et déploiement sur votre VM
[Lien vers la VM - version TP2](http://192.168.75.78:8080/v1/)

# TP3

### 2.1 Pattern contexte

 Proposez une solution pour intercepter l'IOException potentiellement levée par le chargement de données.
 -> Nous avons regardé dans le terminal pour vérifier si lorqsue la liste des canditats ne se charge pas nous avons bien l'erreur.


### 2.2 Pattern chaîne de responsabilité

Nous avons ajouté les filtres dans un dossier filter. Il fonctionnent bien. En premier le filtre Authentification puis le filtre Admin.

### 2.3 Pattern MVC

Nous avons réfacoré notre application. Chaque clic est désormais intercepté par notre contrôleur. Nous avons mis en place la gestion de nos redirections (internes/externes). Nous avons également ajusté notre code car avec les redirection "RequestDispatcher.include()" le content type pour les includes html ainsi que l'encodage UTF-8 sur la redirection posaient problème. ([infos](https://perso.liris.cnrs.fr/lionel.medini/enseignement/M1IF03/#md=TP/md/encodage)). Les jsp n'ont plus qu'une fonction d'affichage. Nous avons utilisé la méthode EL avec "RequestScope" pour récupérer les données nécéssaires pour l'affichage.

Au final, à quoi vous sert le contrôleur principal ?
-> Le controleur sécurise notre apllication, les jsp ne sont plus accessibles depuis l'URL. Notre contrôleur s'occupe de toutes les redirections en fonction des cas d'utilisation.

### 3.1 Utilisation des en-têtes HTTP de date

Nous avons ajouté un filter. Il renvoie bien le code 304 sur la page listballots (admin).

### 3.2 Utilisation d'Entity Tags (ETag)

Notre ETag est contruit de la manière suivante :
Le ballot convertit en string + le nombre courant de votes
Si il n'y a aucun ballot l'ETag est : "vide+0"

La page ballot.jsp s'actualise toutes les 5 secondes, et lors des GET, nous avons bien le code 304 lorque la page s'actualise toute seule.


## Intégration continue et déploiement sur votre VM
[Lien vers la VM - version TP3](http://192.168.75.78:8080/v2/)


# TP4

### 1.1. Contrôleurs de ressources

Pour l'API nous nous sommes basés sur le fichier swagger-lite fournit par L. MEDINI sur le RocketChat.
Nous avons mit en place deux contrôleurs principaux : election et users.
Ces contrôleurs principaux renvoient sur les contrôleurs délégués pour répondre aux attentes spécifiques de l'API.
Nous avons ajouté aux endroits nécessaires les méthodes GET, POST, PUT et DELETE.
Une map d'utilisateurs et une map de ballots ont étés crées pour correspondre aux besoins de l'API. 

### 1.2 Modification des URLs

Nous avons découpés des uri en plusieurs morceaux grâce à request.getRequestURI() et split().
Les cas pour lesquels il est attendu un json, renvoient directement les élements correspondants.
Nous avons egalement pris en compte la redirection 303 (See Other) à partir du contolleur user, pour le cas "Retrouver le ballot d'un utilisateur", vers le contolleur election.


### 2.1. Contexte des requêtes

Tout ce qui est liée à la session http a été supprimé des servelts et filters.
Desormais, nous récupérons l'utilisateur par le header des requetes envoyés par swagger.
Dans les cas où il y a un cadenas sur swagger, nous mettons une string dans le champ Authorization. Celle-ci est ensuite récupérée dans authentification filter. Le token est ensuite vérifié puis placé en attibut de requete. Ainsi, les servelts ont l'information sur un utilisateur en récupérant la récupérant dans l'attribut de requete.


### 2.2 Authentification Stateless

Nous avons remplacé la création de session par celle d'un token "simple" JWT.
Pour le générer, il faut se logger une première fois. Enfin, la string est récupérable dans le header Authoriation de la réponse.

### 4.2. Documentation de l'API

L'api est testable en localhost avec swager et postman.

https://editor.swagger.io/


## Intégration continue et déploiement sur votre VM
[Lien vers la VM - version TP4](http://192.168.75.78:8080/v3/)



# TP5 Programmation côté client (requêtage asynchrone)


### Client pour votre API "Election M1IF03"

Nous avons mis en place la SPA. Les requetes sont dirigées vers l'api de M. MEDINI pour recuperer les json.
Tous les cas ont été pris en compte. Nous avons utilisé des scripts pour faire les requetes Fetch. Notre SPA est maintenant pleinement fonctionnelle.
Nous n'avons pas utilisé Mustache ni JQuery. Nous avons ajouté une feuille et le javascript materialize pour le design ainsi que le CSS. 


## Intégration continue et déploiement sur votre VM
[Lien vers la VM - version TP5](http://192.168.75.78:8080/api/client)




# TP 7

## 1. Analyse de l'état initial de votre application
### déploiement sur Tomcat

Test depuis un ordinateur de la FAC :
 temps de chargement de la page HTML : 37.5 ms
 le temps d'affichage de l'app shell : 132 ms
 le temps d'affichage du chemin critique de rendu (CRP) : 156 ms


 ### 2. Déploiement des fichiers statiques sur nginx

Test depuis un ordinateur de la FAC :
 temps de chargement de la page HTML : 15.39 ms
 le temps d'affichage de l'app shell : 110 ms
 le temps d'affichage du chemin critique de rendu (CRP) : 122 ms

### Ameliorations

Test depuis un ordinateur de la FAC : 
 temps de chargement de la page HTML :
 Le taux d' évolution de 37.5 à 15.39 est -58.96 % (((15.39 - 37.5) / 37.5) * 100 = -58.96 % )

 le temps d'affichage de l'app shell :
 Le taux d' évolution de 132 à 110 est -16.67 % ((110 - 132) / 132) * 100 = -16.67 % )

 le temps d'affichage du chemin critique de rendu (CRP) :
 Le taux d' évolution de 156 à 122 est -21.79 % ((122 - 156) / 156) * 100 = -21.79 % 



