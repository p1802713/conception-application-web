package fr.univlyon1.m1if.m1if03.servlets;

import javax.servlet.ServletException;

import fr.univlyon1.m1if.m1if03.classes.User;
import fr.univlyon1.m1if.m1if03.utils.ElectionM1if03JwtHelper;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;

@WebServlet(name="Home", urlPatterns = {})
public class Home extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {  
        String login = req.getParameter("login");
        if(login != null && !login.equals("")) {
            Map<String, User> utilisateur = (Map<String, User>) req.getServletContext().getAttribute("utilisateur");
            User x = new User(login,
                    req.getParameter("nom") != null ? req.getParameter("nom") : "",
                    req.getParameter("admin") != null && req.getParameter("admin").equals("on"), utilisateur.size());
            utilisateur.put(login, x);
            String token = ElectionM1if03JwtHelper.generateToken(x.getLogin(), x.isAdmin(), req);
            resp.setHeader("Authorization", "Bearer " + token);
            req.getRequestDispatcher("/WEB-INF/components/electionHome.jsp").forward(req, resp);
        }
    }
}