package fr.univlyon1.m1if.m1if03.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univlyon1.m1if.m1if03.classes.Ballot;
import fr.univlyon1.m1if.m1if03.classes.Bulletin;
import fr.univlyon1.m1if.m1if03.classes.Candidat;
import fr.univlyon1.m1if.m1if03.classes.User;


import org.json.simple.JSONArray;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;


@WebServlet(name="Ballots", urlPatterns = {})
public class Ballots extends HttpServlet {
    Map<String, Ballot> ballots;
    List<Bulletin> bulletins;
    Map<String, Candidat> candidats;
    Map<String, User> utilisateur;

    @Override
    @SuppressWarnings("unchecked")
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        candidats = (Map<String, Candidat>) config.getServletContext().getAttribute("candidats");
        System.out.println("Candidats : " + candidats.size());

        bulletins = (List<Bulletin>) config.getServletContext().getAttribute("bulletins");
        System.out.println("Bulletins : " + bulletins.size());

        ballots = (Map<String, Ballot>) config.getServletContext().getAttribute("ballots");
        System.out.println("Ballots : " + ballots.size());

        utilisateur = (Map<String, User>) config.getServletContext().getAttribute("utilisateur");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        String action = req.getRequestURL().toString();
        String[] split = action.split("/");
        String login = (String) req.getAttribute("loginConnectedUser");
        ObjectMapper objectMapper = new ObjectMapper();
        PrintWriter printWriter = resp.getWriter();
        User user = utilisateur.get(login);
        //Cas GET election/ballots  "La liste te tous les ballots, uniquement pour l'admin"
        if (action.endsWith("/ballots") && split.length == 6) {
            if(user.isAdmin()) {
                JSONArray json = new JSONArray();
                for (String nom : utilisateur.keySet()) {
                    if(ballots.get(utilisateur.get(nom).getLogin())!=null){  
                        json.add(action + "/" + String.valueOf(utilisateur.get(nom).getidBallot()));
                    }
                }
                printWriter.println(objectMapper.writeValueAsString(json));
                resp.setContentType("application/json");
                printWriter.close();
            } else {
                resp.sendError(403);
                return;
            }
        }

        // Cas GET election/ballot/byUser/{login}
        //Exemple  "http://localhost:8080/election/ballots/1"
        else if (split.length == 8 && (split[7].equals(user.getLogin()) || user.isAdmin())) {
            JSONArray json = new JSONArray();
            String url = "";
            for(int i=0; i<5; i++){
                url = url + split[i] + "/";
            } 
            json.add(url + "ballots/" + utilisateur.get(split[7]).getidBallot());
            printWriter.println(objectMapper.writeValueAsString(json));
            resp.setContentType("application/json");
            printWriter.close();
        }
        // Cas GET election/ballots/{ballotID} "Le ballot d'un utilisateur"
        //Exemple   "http://localhost:8080/election/vote/0"
        else if(split[5].equals("ballots") && split.length == 7){ 
            int i = 0;
            String url = "";
            for(int j=0; j<5; j++){
                url = url + split[j] + "/";
            } 
            for (String nom : utilisateur.keySet()) {
                try {
                    int s = Integer.parseInt(split[6]);
                    if(utilisateur.get(nom).getidBallot() == s) {  
                        for (String nomCandidat : candidats.keySet()) {
                            if(ballots.get(utilisateur.get(nom).getLogin()).getBulletin().getCandidat().getNom().equals(nomCandidat)) {
                                break;
                            }
                            i++;
                        }
                        break;
                    }
                } catch(NumberFormatException e) {
                    resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "id doit etre un int");
                    return;
                }
            }
            JSONArray json = new JSONArray();
            json.add(url + "vote/"+i);
            printWriter.println(objectMapper.writeValueAsString(json));
            resp.setContentType("application/json");
            printWriter.close();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String action = req.getRequestURL().toString();
        String[] split = action.split("/");

        String log = (String) req.getAttribute("loginConnectedUser");
        utilisateur = (Map<String, User>) req.getServletContext().getAttribute("utilisateur");
        User user = utilisateur.get(log);

        if (split[6].equals(user.getLogin()) || user.isAdmin()) {

            String login = split[6];
            Ballot ballot = ballots.get(login);
            Bulletin bulletin = ballot.getBulletin();
            bulletins.remove(bulletin);
            ballot.setBulletin(null);
            ballots.remove(login);

            req.getRequestDispatcher("/WEB-INF/components/electionHome.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String candidat = request.getParameter("nomCandidat");
        if (candidat == null || candidat.equals("")) {
            response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "Cette application ne prend pas encore en charge le vote blanc.");
            return;
        }

        String login = (String) request.getAttribute("loginConnectedUser");
        utilisateur = (Map<String, User>) request.getServletContext().getAttribute("utilisateur");
        User user = utilisateur.get(login);
        Bulletin bulletin = new Bulletin(candidats.get(candidat));
        bulletins.add(bulletin);
        Ballot ballot = new Ballot(bulletin);
        ballots.put(user.getLogin(), ballot);

        request.getRequestDispatcher("/WEB-INF/components/electionHome.jsp").forward(request, response);

    }
}