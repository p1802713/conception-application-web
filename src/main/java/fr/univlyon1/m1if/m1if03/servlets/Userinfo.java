package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.User;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject; 
import java.io.IOException;
import java.util.Map;
import java.io.PrintWriter;


@WebServlet(name = "Userinfo", value = {})
public class Userinfo extends HttpServlet {
    Map<String, User> utilisateur;
        
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        utilisateur = (Map<String, User>) req.getServletContext().getAttribute("utilisateur");
        String action = req.getRequestURL().toString();
        String[] split = action.split("/");
        PrintWriter printWriter = resp.getWriter();

        if(action.endsWith("ballot")  && split.length == 7) {
            try {
                String id = split[5];
                resp.setStatus(HttpServletResponse.SC_SEE_OTHER);
                resp.sendRedirect("../../election/ballots/" + id);
            } catch(NumberFormatException e) {
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Oups cette page n'hésite pas encore");
                return;
            }
        } else {
            if (split.length == 6) {
                try {
                    String id = split[5];
                    String login = (String) req.getParameter("loginConnectedUser");
                    User util = utilisateur.get(login);
                    if(util.isAdmin() || util.getLogin().equals(id)) {
                        User x = utilisateur.get(id);
                        JSONObject json = new JSONObject();
                        json.put("login", split[5]); 
                        json.put("nom", x.getNom()); 
                        json.put("admin", x.isAdmin());   
                        printWriter.println(json);
                        resp.setContentType("application/json");
                        printWriter.close();
                        req.getRequestDispatcher("/WEB-INF/components/resultats.jsp").forward(req, resp);
                    }
                } catch(NumberFormatException e) {
                    resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Oups cette page n'hésite pas encore");
                    return;
                }
            }
        }
    }
}