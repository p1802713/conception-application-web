package fr.univlyon1.m1if.m1if03.servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/users/*")
public class Users extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getRequestURI().replace(req.getContextPath() + "/users/", "");
        req.setAttribute("action", action); // Utilisé dans electionHome.jsp
        switch(action) {
            case "login":
                this.getServletContext().getNamedDispatcher("Home").forward(req, resp);
                break;
            case "logout":
                this.getServletContext().getNamedDispatcher("Deco").forward(req, resp);
                break;
            default:
                this.getServletContext().getNamedDispatcher("Userinfo").forward(req, resp);
                break;
        }
    }
}
