package fr.univlyon1.m1if.m1if03.servlets;

import fr.univlyon1.m1if.m1if03.classes.Candidat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject; 
import org.json.simple.JSONArray;  
import java.io.IOException;
import java.util.Map;
import java.io.PrintWriter;

import com.fasterxml.jackson.databind.ObjectMapper;

@WebServlet(name = "Candidats", value = {})
public class Candidats extends HttpServlet {
    Map<String, Candidat> candidats;
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        candidats = (Map<String, Candidat>) req.getServletContext().getAttribute("candidats");

        String action = req.getRequestURL().toString();
        String[] split = action.split("/");

        ObjectMapper objectMapper = new ObjectMapper();
        PrintWriter printWriter = resp.getWriter();

        if(action.endsWith("candidats")  && split.length == 6){
            JSONArray json = new JSONArray();  
            for(int i=0; i<candidats.size();i++){
                json.add(action + "/" + String.valueOf(i));
            }
            printWriter.println(objectMapper.writeValueAsString(json));
        } else {
            if (action.contains("/election/candidats/") && action.endsWith("noms") && split.length == 7) {
                JSONArray json = new JSONArray(); 
                for (String nomCandidat : candidats.keySet()) {
                    json.add(nomCandidat);    
                }
                printWriter.println(objectMapper.writeValueAsString(json));
            } else {
                try {
                    int id = Integer.parseInt(split[split.length - 1]);
                    if (id  < 0 || candidats.size() - 1 < id) {
                        resp.sendError(404);
                        return;
                    }
                    else {
                        if (action.contains("/election/candidats/") && split.length == 7) {
                            int i = 0;
                            for (String nomCandidat : candidats.keySet()) {
                                if(id == i){
                                    JSONObject json = new JSONObject();
                                    json.put("prenom", candidats.get(nomCandidat).getPrenom()); 
                                    json.put("nom", nomCandidat); 
                                    printWriter.println(json);
                                    break;
                                }
                                i++;
                            }
                        } else {
                            resp.sendError(404);
                            return;
                        }  
                    }
                } catch(NumberFormatException e) {
                    resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Oups cette page n'hésite pas encore");
                    return;
                }
            }
        } 
        resp.setContentType("application/json");
        printWriter.close();
        req.getRequestDispatcher("/WEB-INF/components/resultats.jsp").forward(req, resp);
    }

}