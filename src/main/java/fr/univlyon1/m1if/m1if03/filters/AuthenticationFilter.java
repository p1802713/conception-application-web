package fr.univlyon1.m1if.m1if03.filters;


import javax.servlet.*;
import fr.univlyon1.m1if.m1if03.utils.ElectionM1if03JwtHelper;
import javax.servlet.annotation.*;
import javax.servlet.http.*;
import com.auth0.jwt.exceptions.JWTVerificationException;
import sun.tools.jconsole.JConsole;

import java.io.IOException;

@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/*")
public class AuthenticationFilter extends HttpFilter {
    private final String[] authorizedURIs = {  "/client", "/users/login", "/index.html", "/static", "/election/resultats", "/election/candidats"};
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String currentUri = req.getRequestURI().replace(req.getContextPath(), "");

        // Traitement de l'URL racine
        if (currentUri.equals("/")) {
            res.sendRedirect("index.html");
            return;
        }

        // Traitement des autres URLs autorisées sans authentification
        for (String authorizedUri : authorizedURIs) {
            if (currentUri.startsWith(authorizedUri)) {
                super.doFilter(req, res, chain);
                return;
            }
        }


        String token = req.getHeader("Authorization");


        if (token != null && token.startsWith("Bearer ")) {
            token = token.replace("Bearer ", "");
            try {
                String login = ElectionM1if03JwtHelper.verifyToken(token, req);
                req.setAttribute("loginConnectedUser", login);
                req.setAttribute("token", token);
                super.doFilter(req, res, chain);
                return;
            } catch (JWTVerificationException | NullPointerException e) {
                res.sendError(401);
            }
        } else {  
            super.doFilter(req, res, chain);
        }
    }
}